# Référencement spatial de nuages de points 3D dans le cadre du chantier scientifique de restauration de Notre-Dame de Paris

Dans le cadre de la restauration de la cathédrale Notre-Dame de Paris, ce projet a pour but de trouver une solution permettant d'effectuer un recalage de différentes scènes dans un repère unique, utilisant des méthodes automatiques et/ou semi-automatiques. Ces nuages de points volumineux ont été construits à partir d'acquisitions LIDAR. Ce projet est divisé en deux parties : une partie recherche et une partie développement.
La partie recherche consiste à trouver la meilleure solution à partir d'un état de l'art des articles scientifiques sur le sujet. La plupart des articles sur ce sujet se penchent sur l'aspect 3D, c'est pourquoi l'algorithme développé dans la partie développement est une méthode semi-automatique, qui effectue un recalage 3D entre deux nuages de points utilisant l'algorithme RANSAC ainsi que la méthode Iterative Closest Point.

