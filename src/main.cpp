#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>
#include <pcl/registration/icp.h>
#include <pcl/filters/filter.h>
#include <pcl/common/transforms.h>

int main()
{
    // Initialize two point clouds
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud1 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ> finalCloud;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloudOut (new pcl::PointCloud<pcl::PointXYZ>);

    // Paths to those point clouds
    const std::string path1 = "../data_bunny/data/bun045.ply";
    const std::string path2 = "../data_bunny/data/bun090.ply";

    // std::cout << std::isfinite("nan");

    // Load the first file
    if (pcl::io::loadPLYFile<pcl::PointXYZ> (path1, *cloud1) == -1)
    {
      PCL_ERROR ("Couldn't read file \n");
      return (-1);
    }
    std::cout << "Loaded "
              << cloud1->width * cloud1->height
              << " data points from " + path1 //+" with the following fields: "
              << std::endl;
    /*for (size_t i = 0; i < cloud1->points.size (); ++i)
      std::cout << "    " << cloud1->points[i].x
                << " "    << cloud1->points[i].y
                << " "    << cloud1->points[i].z << std::endl; */

    // Load the second file
    if (pcl::io::loadPLYFile<pcl::PointXYZ> (path2, *cloud2) == -1) //* load the file
    {
      PCL_ERROR ("Couldn't read file \n");
      return (-1);
    }
    std::cout << "Loaded "
              << cloud2->width * cloud2->height
              << " data points from " + path2 //+" with the following fields: "
              << std::endl;

    if(cloud1->size() > 0 && cloud2->size() > 0){
        std::cout << "Clouds are not empty" << std::endl;


        //remove NaN points from cloud1 by iteration
        pcl::PointCloud<pcl::PointXYZ>::iterator it = cloud1->points.begin();
        while (it != cloud1->points.end())
        {
            float x, y, z;
            x = it->x;
            y = it->y;
            z = it->z;
            //std::cout << "x: " << x << "  y: " << y << "  z: " << z <<  std::endl;
            if (!std::isfinite(x) || !std::isfinite(y) || !std::isfinite(z))
            {
                it = cloud1->points.erase(it);
            }
            else{
                //std::cout << "x: " << x << "  y: " << y << "  z: " << z <<  std::endl;
                ++it;
            }
        }

        //remove NaN points from cloud2 by iteration
        pcl::PointCloud<pcl::PointXYZ>::iterator it2 = cloud2->points.begin();
        while (it2 != cloud2->points.end())
        {
            float x, y, z;
            x = it2->x;
            y = it2->y;
            z = it2->z;
            //std::cout << "x: " << x << "  y: " << y << "  z: " << z <<  std::endl;
            if (!std::isfinite(x) || !std::isfinite(y) || !std::isfinite(z))
            {
                it2 = cloud2->points.erase(it2);
            }
            else
                ++it2;
        }

/*
        //remove NaN points from cloud1
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud1f(new pcl::PointCloud<pcl::PointXYZ>);
        std::vector<int> indices;
        cloud1->is_dense = false;
        pcl::removeNaNFromPointCloud(*cloud1,*cloud1f, indices);

        //remove NaN points from cloud2
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2f(new pcl::PointCloud<pcl::PointXYZ>);
        std::vector<int> indices2;
        cloud2->is_dense = false;
        pcl::removeNaNFromPointCloud(*cloud2,*cloud2f, indices2);
*/
/*
        //shows non NaN Points
        pcl::PointCloud<pcl::PointXYZ>::iterator itf = cloud1->points.begin();
        while (itf != cloud1->points.end())
        {
            float x, y, z;
            x = itf->x;
            y = itf->y;
            z = itf->z;
            if (!std::isfinite(x) || !std::isfinite(y) || !std::isfinite(z))
            {
                std::cout << "x: " << x << "  y: " << y << "  z: " << z <<  std::endl;
            }
            ++itf;
        }

*/

        //ICP
        pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
        icp.setInputSource(cloud1);
        icp.setInputTarget(cloud2);

        icp.setMaximumIterations(500);
        icp.setTransformationEpsilon(1e-9);
        icp.setMaxCorrespondenceDistance(0.05);
        icp.setEuclideanFitnessEpsilon(1);
        icp.setRANSACOutlierRejectionThreshold(1.5);


        std::cout << "ICP will start now.\n";

        icp.align(finalCloud);

        std::cout << "ICP finished.\n";

        if (icp.hasConverged()){
            Eigen::Matrix4f transformationMatrix = icp.getFinalTransformation ();
            std::cout << "ICP converged." << std::endl
                      << "The score is " << icp.getFitnessScore() << std::endl;
            std::cout << "Transformation matrix : " << std::endl;
            std::cout << transformationMatrix << std::endl;

            pcl::transformPointCloud( *cloud2, *cloudOut, transformationMatrix);

            finalCloud.width=1;
            finalCloud.height=finalCloud.points.size();

            pcl::PointCloud<pcl::PointXYZ> transformed_target_cloud;
            //pcl::transformPointCloud(finalCloud, transformed_target_cloud, transformationMatrix);

            pcl::PointCloud<pcl::PointXYZ> transformed_input_cloud;
            Eigen::Isometry3f pose(Eigen::Isometry3f(Eigen::Translation3f(
                                  cloud1->sensor_origin_[0],
                                  cloud1->sensor_origin_[1],
                                  cloud1->sensor_origin_[2])) *
                                  Eigen::Isometry3f(cloud1->sensor_orientation_));
            transformationMatrix = pose.matrix();
            pcl::transformPointCloud(*cloud1, transformed_input_cloud, transformationMatrix);

            // now both point clouds are transformed to the origin and can be added up
            pcl::PointCloud<pcl::PointXYZ> output_cloud;
            output_cloud += transformed_input_cloud;
            output_cloud += *cloudOut;

            pcl::io::savePCDFileASCII("../transinput_cloudOut.pcd", output_cloud);

        }
        else std::cout << "ICP did not converge." << std::endl;


    }

    return (0);
}

