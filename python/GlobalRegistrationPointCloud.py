# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 13:26:47 2020

@author: pyanz
"""

import open3d as o3d
import copy
import numpy as np
#import time


def extract_local_from_global(source, target, source_coords, target_coords):
    """
    --------ENGLISH--------
    (User has previously selected a pair of corresponding points in two point clouds, and the coordinates of those points have been saved.)
    Function that, according to the selected corresponding points in a local scene and a global scene, extracts a smaller scene from 
    the global scene that represents the local scene.
    This extract is saved in the file result/target_extract.ply
    
    -------FRANCAIS--------
    (L'utilisateur a préalablement sélectionné une paire de points homologues entre deux nuages de points,
    et les coordonnées de ces points ont été notées.)
    Fonction qui, selon la paire de coordonnées sélectionnées sur une scène locale et la scène globale,
    extrait de la scène globale une scène plus petite correspondant au nuage de points local.
    Cet extrait est enregistré dans le fichier result/target_extract.ply
    
    Parameters
    ----------
    source : local point cloud // nuage de points local
    target : global point cloud // nuage de points global
    source_coords : (list) coordinates of the corresponding points in the local scene // coordonnées sélectionnées d'un point sur le nuage local
    target_coords : (list) coordinates of the corresponding points in the global scene //  sélectionnées du même point sur le nuage global
    
    Returns
    -------
    pcd : (Open3D.o3D.geometry.PointCloud) point cloud extracted from the global scene // Nuage de points local extrait du nuage global
    
    """
    
    # Convert Open3D.o3d.geometry.PointCloud to numpy array
    source_arr = np.asarray(source.points)
    target_arr = np.asarray(target.points)
    
    # Compute search radius (dist_max) for target point cloud
    xs, ys, zs = source_coords
    dist_max = 0
    for point in source_arr:
        z, x, y = point     # source point cloud extracted using CloudCompare has this order for coordinates // to be adjusted according to data type
        dist = np.sqrt((xs-x)**2 + (ys-y)**2 + (zs-z)**2)
        if dist > dist_max:
            dist_max = dist
    
    # Extract points from target point cloud around target_coords within search radius
    xt, yt, zt = target_coords
    xyz_list = []
    for point in target_arr:
        x, y, z = point     # target point cloud has regular order for coordinates
        dist = np.sqrt((xt-x)**2 + (yt-y)**2 + (zt-z)**2)
        if dist_max >= dist :
            xyz_list.append(point)
    xyz = np.asarray(xyz_list)
    
    
    # Pass xyz to Open3D.o3D.geometry.PointCloud
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(xyz)
    
    # Write new ply file
    o3d.io.write_point_cloud("../result/target_extract.ply", pcd)
    
    # Visualization
    # pcd_load = o3d.io.read_point_cloud("../result/target_extract.ply")
    # o3d.visualization.draw_geometries([pcd_load])
    
    pcd_fpfh = preprocess_point_cloud(pcd)
    
    return pcd, pcd_fpfh
    


def draw_registration_result(source, target, transformation):
    """
    --------ENGLISH--------
    Function that vizualises two point clouds using a transformation matrix
    
    -------FRANCAIS--------
    Fonction qui permet de visualiser deux nuages de points en prenant en compte une matrice de transformation

    Parameters
    ----------
    source : first point cloud // premier nuage de points
    target : second point cloud // deuxième nuage de points
    transformation : transformation matrix // matrice de transformation

    Returns
    -------
    None.

    """
    source_temp = copy.deepcopy(source)
    target_temp = copy.deepcopy(target)
    source_temp.paint_uniform_color([1, 0.706, 0])
    target_temp.paint_uniform_color([0, 0.651, 0.929])
    source_temp.transform(transformation)
    o3d.visualization.draw_geometries([source_temp, target_temp])
    
    
def preprocess_point_cloud(pcd):
    """
    --------ENGLISH--------
    Function that preprocesses a point cloud file and extracts feature points using the FPFH method
    
    -------FRANCAIS--------
    Fonction qui prétraite un nuage de point en entrée et en sort les points d'intérêt utilisant la méthode FPFH

    Parameters
    ----------
    pcd : point cloud to preprocess // nuage de points à prétraiter
    
    Returns
    -------
    pcd_fpfh : feature points found // points d'intérêt trouvés

    """
    
    radius_normal = 0.01 #10cm
    pcd.estimate_normals(
        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))

    radius_feature = 0.025
    print(":: Compute FPFH feature with search radius %.3f." % radius_feature)
    pcd_fpfh = o3d.registration.compute_fpfh_feature(
        pcd,
        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_feature, max_nn=100))
    return pcd_fpfh


def prepare_dataset(source_path, target_path):
    """
    --------ENGLISH--------
    Function that prepares the dataset using the paths to the two point cloud files to register
    
    -------FRANCAIS--------
    Fonction qui prépare les données en utilisant les chemins vers les deux nuages de points à recaler

    Parameters
    ----------
    source_path : path to first point cloud // chemin vers le premier nuage de points
    target_path : path to second point cloud // chemin vers le deuxième nuage de points
    
    Returns
    -------
    source : (open3d.open3d_pybind.geometry.PointCloud) first point cloud in Open3D format // premier nuage de points dans le format Open3D
    target : (open3d.open3d_pybind.geometry.PointCloud) second point cloud in Open3D format // deuxième nuage de points dans le format Open3D
    source_fpfh : (open3d.open3d_pybind.registration.Feature) first point cloud's feature points // points d'intérêt du premier nuage
    target_fpfh : (open3d.open3d_pybind.registration.Feature) second point cloud's feature points // points d'intérêt du deuxième nuage

    """
    
    print(":: Load two point clouds and disturb initial pose.")
    source = o3d.io.read_point_cloud(source_path)
    target = o3d.io.read_point_cloud(target_path)

    # transformation matrix to disturb initial pose
    trans_init = np.asarray([[0.0, 0.0, 1.0, 0.0], [1.0, 0.0, 0.0, 0.0],
                              [0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 0.0, 1.0]])
    source.transform(trans_init)
    
    
    # Visualization 
    #draw_registration_result(source, target, np.identity(4))

    source_fpfh = preprocess_point_cloud(source)
    target_fpfh = preprocess_point_cloud(target)
    
    return source, target, source_fpfh, target_fpfh


def execute_global_registration(source, target, source_fpfh, target_fpfh):
    """
    --------ENGLISH--------
    Function that uses RANSAC to roughly register two point clouds 
    
    -------FRANCAIS--------
    Fonction qui utilise RANSAC pour recaler grossièrement deux nuages de points

    Parameters
    ----------
    source : first point cloud to register // premier nuage de points à recaler
    target : second point cloud to register // deuxième nuage de points à recaler
    source_fpfh : feature points from source // points d'intérêt du nuage source
    target_fpfh : feature points from target // points d'intérêt du nuage target
    
    Returns
    -------
    result : (open3d.registration.RegistrationResult) registration result with fitness score, 
        RMSE of all inlier correspondences (the lower the better), and correspondence set between 
        source and target point cloud

    """
    
    distance_threshold = 0.0075
    print(":: RANSAC registration on downsampled point clouds.")
    print("   we use a liberal distance threshold %.3f." % distance_threshold)
    
    result = o3d.registration.registration_ransac_based_on_feature_matching(
        source, target, source_fpfh, target_fpfh, distance_threshold,
        o3d.registration.TransformationEstimationPointToPoint(False),
        4, [
            o3d.registration.CorrespondenceCheckerBasedOnEdgeLength(
                0.9),
            o3d.registration.CorrespondenceCheckerBasedOnDistance(
                distance_threshold)
        ], o3d.registration.RANSACConvergenceCriteria(4000000, 1000))
    return result


def refine_registration(source, target, source_fpfh, target_fpfh, result_ransac):
    """
    --------ENGLISH--------
    Function that uses ICP to apply a refine registration on two point clouds
    
    -------FRANCAIS--------
    Fonction qui utilise la méthode ICP pour effectuer un recalage précis entre deux nuages de points

    Parameters
    ----------
    source : first point cloud to register // premier nuage de points à recaler
    target : second point cloud to register // deuxième nuage de points à recaler
    source_fpfh : feature points from source // points d'intérêt du nuage source
    target_fpfh : feature points from target // points d'intérêt du nuage target
    result_ransac : previous ransac results // résultats de ransac préalables
    
    Returns
    -------
    result : (open3d.registration.RegistrationResult) registration result with fitness score, 
        RMSE of all inlier correspondences (the lower the better), and correspondence set between 
        source and target point cloud

    """
    
    distance_threshold = 0.002
    
    radius_normal = 0.01
    source.estimate_normals(
        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))
    target.estimate_normals(
        o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))
    
    print(":: Point-to-plane ICP registration is applied on original point")
    print("   clouds to refine the alignment. This time we use a strict")
    print("   distance threshold %.3f." % distance_threshold)
    result = o3d.registration.registration_icp(
        source, target, distance_threshold, result_ransac.transformation,
        o3d.registration.TransformationEstimationPointToPlane())
    return result





if __name__ == "__main__" :
    
    source_path = "../data_bunny/data/bun090_seg.ply"
    target_path = "../data_bunny/data/bun045.ply"
    
    source, target, source_fpfh, target_fpfh = prepare_dataset(source_path, target_path)
    
    #Extract local scene from global scene
    new_target, new_target_fpfh = extract_local_from_global(source, target, [-0.024,0.12,0.0275], [0.0485,0.1184,0.076])
    
    # result_ransac = execute_global_registration(source, target, source_fpfh, target_fpfh) #without running extract_local_from_global
    result_ransac = execute_global_registration(source, new_target, source_fpfh, new_target_fpfh) #after running extract_local_from_global
    
    print("\nRANSAC results : ")
    print(result_ransac)
    print("RANSAC transformation matrix : ")
    print(result_ransac.transformation)
    
    # RANSAC result visualization
    #draw_registration_result(source, target, result_ransac.transformation)
    
    # result_icp = refine_registration(source, target, source_fpfh, target_fpfh, result_ransac) #without running extract_local_from_global
    result_icp = refine_registration(source, new_target, source_fpfh, new_target_fpfh, result_ransac) #after running extract_local_from_global
    
    print("\nICP results : ")
    print(result_icp)
    
    print(":: ICP Transformation matrix :")
    print(result_icp.transformation)
    
    # ICP result visualization
    #draw_registration_result(source, target, result_icp.transformation)
    
























