# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""


import pylas 
from pathlib import Path
import os

base_path = Path(__file__).parent
file_path = str((base_path / "data/pointclouds/").resolve()) + "/"

files_list = os.listdir(file_path)

for file in files_list :
    file_name = file.split(".")[0]
    print(file_name)
    laz = pylas.read(file_path + file)
    las = pylas.convert(laz)
    Path(base_path / "data/pointclouds_las/").mkdir(parents=True,exist_ok=True)
    las_path = str((base_path / "data/pointclouds_las/").resolve()) + "/"
    las.write(las_path + file_name + ".las")